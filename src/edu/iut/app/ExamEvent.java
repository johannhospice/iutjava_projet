package edu.iut.app;

import java.util.ArrayList;
import java.util.Date;

/**
 * Objet contenant des information sur un �v�nement
 */
public class ExamEvent {
	public ExamEvent() {
	}

	public ExamEvent(Date date, Person person, ArrayList<Person> jury, String classRoom, ArrayList<Document> document) {
		this.examDate = date;
		this.student = person;
		this.classroom = classRoom;
		this.documents = document;
		this.jury = jury;
	}

	public ExamEvent(Date date, Person person, ArrayList<Person> jury, String classRoom) {
		this.examDate = date;
		this.student = person;
		this.jury = jury;
		this.classroom = classRoom;
	}

	public void setDate(Date date) {
		this.examDate = date;
	}

	public Date getDate() {
		return examDate;
	}

	public void setStudent(Person student) {
		this.student = student;
	}

	public Person getStudent() {
		return student;
	}

	public void setJury(ArrayList<Person> jury) {
		this.jury = jury;
	}

	public ArrayList<Person> getJury() {
		return jury;
	}

	public void setClassroom(String classroom) {
		this.classroom = classroom;
	}

	public String getClassroom() {
		return classroom;
	}

	public void setDocuments(ArrayList<Document> documents) {
		this.documents = documents;
	}

	public ArrayList<Document> getDocuments() {
		return documents;
	}

	protected Date examDate;
	protected Person student;
	protected ArrayList<Person> jury;
	protected String classroom;
	protected ArrayList<Document> documents;

	public void setInfo(ExamEvent info) {
		examDate = info.examDate;
		student = info.student;
		jury = info.jury;
		classroom = info.classroom;
		documents = info.documents;

	}

}
