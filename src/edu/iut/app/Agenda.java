package edu.iut.app;

import edu.iut.app.ApplicationSession;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;
/**
 * Liste d'�venements
 */
public class Agenda extends LinkedList<ExamEvent> {
	public Agenda() {
		super();
	}

	public void addEvent(ExamEvent examEvent) {
		add(examEvent);
	}

	public int sizeIncludeDate(Date date) {
		int nb = 0;
		for(ExamEvent e: this)
			if (date.getHours() == e.getDate().getHours() && date.getDate() == e.getDate().getDate()
					&& date.getMonth() == e.getDate().getMonth() && date.getYear() == e.getDate().getYear())
				nb++;
		return nb;
	}
/**
 * Fonction permettant la r�ation d'un agenda al�atoirement
 */
	public void build() {
		for (int j = 0; j < Rnd.getRandNum(200, 500); j++) {
			Date date = new Date(2016,0,Rnd.getRandNum(15,17),Rnd.getRandNum(0,24),Rnd.getRandNum(0,60));
			Person student = new Person(Person.PersonFunction.STUDENT, Rnd.generateRandomString(Rnd.getRandNum(5, 10)),
					Rnd.generateRandomString(Rnd.getRandNum(5, 10)),
					Rnd.generateRandomString(Rnd.getRandNum(5, 10)) + '@'
							+ Rnd.generateRandomString(Rnd.getRandNum(5, 10)) + '.'
							+ Rnd.generateRandomString(Rnd.getRandNum(2, 3)),
					"06" + Rnd.generateRandomNum(8));

			ArrayList<Person> jury = new ArrayList<Person>();
			for (int i = 0; i < Rnd.getRandNum(1, 3); i++)
				jury.add(new Person(Person.PersonFunction.JURY, Rnd.generateRandomString(Rnd.getRandNum(5, 10)),
						Rnd.generateRandomString(Rnd.getRandNum(5, 10)),
						Rnd.generateRandomString(Rnd.getRandNum(5, 10)) + '@'
								+ Rnd.generateRandomString(Rnd.getRandNum(5, 10)) + '.'
								+ Rnd.generateRandomString(Rnd.getRandNum(2, 3)),
						"06" + Rnd.generateRandomNum(8)));

			String classroom = new String(Rnd.generateRandomString(3));

			ArrayList<Document> documents = new ArrayList<Document>();
			for (int i = 0; i < Rnd.getRandNum(1, 3); i++)
				documents.add(new Document(Rnd.generateRandomString(Rnd.getRandNum(10, 20))));

			add(new ExamEvent(date, student, jury, classroom, documents));
		}
	}
/**
 * Class permettant de recevoir des information de maniere al�atoire
 */
	private static class Rnd {
		private static final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

		public static String generateRandomString(int lenght) {
			StringBuffer randStr = new StringBuffer();
			randStr.append(CHAR_LIST.charAt(getRandNum(26, CHAR_LIST.length() - 11)));
			for (int i = 1; i < lenght; i++) {
				char ch = CHAR_LIST.charAt(getRandNum(0, 25));
				randStr.append(ch);
			}
			return randStr.toString();
		}

		public static String generateRandomNum(int lenght) {
			StringBuffer randStr = new StringBuffer();
			for (int i = 0; i < lenght; i++) {
				char ch = CHAR_LIST.charAt(getRandNum(CHAR_LIST.length() - 10, CHAR_LIST.length() - 1));
				randStr.append(ch);
			}
			return randStr.toString();
		}

		private static int getRandNum(int min, int max) {
			Random rand = new Random();
			return rand.nextInt(max - min + 1) + min;
		}

		private static long getRandLong() {
			return new Random().nextLong();
		}
	}

	public ArrayList<ExamEvent> getEventByTime(Date begin, Date end) {
		ArrayList<ExamEvent> events = new ArrayList<>();
		for (ExamEvent event : this)
			if (event.getDate().compareTo(begin) <= 0 && event.getDate().compareTo(end) >= 0)
				events.add(event);
		return events;
	}

}
