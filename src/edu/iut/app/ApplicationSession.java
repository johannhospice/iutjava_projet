package edu.iut.app;

import java.util.ResourceBundle;
import java.util.Locale;
import edu.iut.app.Agenda;

/**
 * Objet contant les donn�es de l'application
 */
public class ApplicationSession {

	private static ResourceBundle resourceBundle;
	private String[] minmonths;
	private String[] days;
	private String[] mindays;
	private Agenda agenda;
	private String[] months;

	private static ApplicationSession session = null;

	private ApplicationSession() {
		setLocale(Locale.getDefault());

		agenda = new Agenda();
		agenda.build();

		loadString();
	}

	public void loadString() {
		days = new String[7];
		days[0] = getString("monday");
		days[1] = getString("tuesday");
		days[2] = getString("wednesday");
		days[3] = getString("thursday");
		days[4] = getString("friday");
		days[5] = getString("saturday");
		days[6] = getString("sunday");

		mindays = new String[7];
		mindays[0] = getString("mon");
		mindays[1] = getString("tue");
		mindays[2] = getString("wed");
		mindays[3] = getString("thu");
		mindays[4] = getString("fri");
		mindays[5] = getString("sat");
		mindays[6] = getString("sun");

		minmonths = new String[12];
		minmonths[0] = getString("jan");
		minmonths[1] = getString("feb");
		minmonths[2] = getString("mar");
		minmonths[3] = getString("apr");
		minmonths[4] = getString("may");
		minmonths[5] = getString("jun");
		minmonths[6] = getString("jul");
		minmonths[7] = getString("aug");
		minmonths[8] = getString("sep");
		minmonths[9] = getString("oct");
		minmonths[10] = getString("nov");
		minmonths[11] = getString("dec");

		months = new String[12];
		months[0] = getString("january");
		months[1] = getString("february");
		months[2] = getString("march");
		months[3] = getString("april");
		months[4] = getString("may");
		months[5] = getString("june");
		months[6] = getString("july");
		months[7] = getString("august");
		months[8] = getString("september");
		months[9] = getString("october");
		months[10] = getString("november");
		months[11] = getString("december");
	}

	static public ApplicationSession instance() {
		if (session == null) {
			session = new ApplicationSession();
		}
		return session;
	}

	public void setLocale(Locale locale) {
		Locale.setDefault(locale);
		resourceBundle = ResourceBundle.getBundle("edu.iut.resources.strings.res");
	}

	public static String getString(String key) {
		return resourceBundle.getString(key.toLowerCase());
	}

	public String[] getMinDays() {
		return getClone(mindays);
	}

	public String[] getDays() {
		return getClone(days);
	}

	public String[] getMinMonths() {
		return getClone(minmonths);
	}

	public String[] getMonths() {
		return getClone(months);
	}

	public Agenda getAgenda() {
		return agenda;
	}

	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}

	private String[] getClone(String[] toClone) {
		String[] clone = new String[toClone.length];
		for (int i = 0; i < toClone.length; i++)
			clone[i] = new String("" + toClone[i]);
		return clone;
	}

}
