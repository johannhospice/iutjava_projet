package edu.iut.gui.module;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;
import java.util.Locale;

import edu.iut.gui.listeners.*;
import edu.iut.app.Agenda;
import edu.iut.app.*;
import edu.iut.app.ApplicationSession;
import edu.iut.gui.frames.SchedulerFrame;
/**
 * Class renvoyant la barre de menu pr�construite
 */
@SuppressWarnings("serial")
public class MenuBar extends JMenuBar {

	public MenuBar() {
		JMenu menu;
		JMenuItem menuItem;

		/* File Menu */
		menu = new JMenu(ApplicationSession.getString("file"));
		menuItem = new JMenuItem(ApplicationSession.getString("new"));
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ApplicationSession.instance().setAgenda(new Agenda());
				SchedulerFrame.instance().act();
			}
		});
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));
		menu.add(menuItem);
		menu.insertSeparator(1);
		menuItem = new JMenuItem(ApplicationSession.getString("load"));
		menuItem.addActionListener(new LoadListener());
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK));
		menu.add(menuItem);
		menuItem = new JMenuItem(ApplicationSession.getString("save"));
		menuItem.addActionListener(new SaveListener());
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
		menu.add(menuItem);

		menuItem = new JMenuItem(ApplicationSession.getString("quit"));
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (0 == JOptionPane.showConfirmDialog((Component) arg0.getSource(), "Sure?",ApplicationSession.getString("Quit"),JOptionPane.CLOSED_OPTION))
					System.exit(0);
			}
		});
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK));
		menu.add(menuItem);
		add(menu);

		/* Edit Menu */
		menu = new JMenu(ApplicationSession.getString("tool"));
		JMenu submenu = new JMenu(ApplicationSession.getString("Language"));
		menuItem = new JMenuItem("fr");
		menuItem.addActionListener(new LocaleChangerListener(Locale.FRENCH));
		submenu.add(menuItem);
		menuItem = new JMenuItem("en");
		menuItem.addActionListener(new LocaleChangerListener(Locale.ENGLISH));
		submenu.add(menuItem);
		// Search
		menu.add(submenu);
		submenu = new JMenu(ApplicationSession.getString("search"));
		menu.add(submenu);
		menuItem = new JMenuItem(ApplicationSession.getString("Student"));
		menuItem.addActionListener(new SearchListener(Person.PersonFunction.STUDENT));
		submenu.add(menuItem);
		menuItem = new JMenuItem("Jury");
		menuItem.addActionListener(new SearchListener(Person.PersonFunction.JURY));
		submenu.add(menuItem);

		menuItem = new JMenuItem(ApplicationSession.getString("Goto"));
		menuItem.addActionListener(new GotoListener());
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_DOWN_MASK));
		menu.add(menuItem);
		menu.insertSeparator(1);
		add(menu);

		/* Help Menu */
		menu = new JMenu(ApplicationSession.getString("Help"));
		menuItem = new JMenuItem(ApplicationSession.getString("Display"));
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog((Component) arg0.getSource(), "Not yet implemented", "info",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(ApplicationSession.getString("About"));
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog((Component) arg0.getSource(), "Not yet implemented", "info",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		menu.add(menuItem);
		add(menu);
	}

	public void setColor(Color color) {
		setBackground(color);
	}
}
