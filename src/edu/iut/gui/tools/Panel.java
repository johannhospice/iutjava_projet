package edu.iut.gui.tools;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.*;

public class Panel extends JPanel {
	public Panel(Component... components) {
		for(Component component : components)
			add(component);
	}
}
