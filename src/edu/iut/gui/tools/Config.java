package edu.iut.gui.tools;
/**
 * Une classe ne contenant que des constantes utilis� dans d'autres classe
 */
public interface Config {
	public static final int LARGE_WIDTH_FRAME = 990;
	public static final int LARGE_HEIGHT_FRAME = 900;

	public static final int DEFAULT_WIDTH_FRAME = 620;
	public static final int DEFAULT_HEIGHT_FRAME = 840;
	
	public static final int FONT_TITLE_0 = 50;
	public static final int FONT_TITLE_1 = 40;
	public static final int FONT_TITLE_2 = 30;
	public static final int FONT_CORPS = 20;
	public static final int FONT_EMPHASE = 10;
}
