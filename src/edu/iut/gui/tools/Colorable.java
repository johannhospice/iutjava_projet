package edu.iut.gui.tools;

import java.awt.Color;
/**
 * Interface spécifiant le changement de couleur
 *
 */
public interface Colorable {
	public void setColor(Color color1, Color color2);
}
