package edu.iut.gui.tools;



import java.awt.Font;

import javax.swing.Icon;
import javax.swing.JLabel;

public class Label extends JLabel {

	public double height;
	public double width;
	public Label() {
		super();
	}

	public Label(String text) {
		super(text);
	}

	public Label(Icon image) {
		super(image);
	}

	public Label setLabel(String text, int style, int size) {
		setText(text);
		Font font = new Font("", style, size);
		setFont(font);
		height = getPreferredSize().getHeight();
		width = getPreferredSize().getWidth();
		return this;
	}

	public void setPosition(double width, double height) {
		setBounds((int) width, (int) height, getPreferredSize().width,
				getPreferredSize().height);
	}
}
