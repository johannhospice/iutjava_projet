package edu.iut.gui.tools;


import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
/**
 * Fenetre permettant de bloquer temporairement la fenetre parent
 *
 */
public class FocusFrame extends JDialog {
	public FocusFrame(Component parent, JPanel panel, String title) {
		super(SwingUtilities.windowForComponent(parent),title);
        setLocationRelativeTo(parent);
        setModal(true);
        add(panel);
        setMinimumSize(new Dimension(500, 300));
        pack();
        setVisible(true);
	}
}
