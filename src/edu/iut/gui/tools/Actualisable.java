package edu.iut.gui.tools;
/**
 * Interface permettant le typage d'une actualisation
 *
 */
public interface Actualisable {
	public void act();
}
