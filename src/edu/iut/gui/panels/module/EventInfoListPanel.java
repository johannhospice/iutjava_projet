package edu.iut.gui.panels.module;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import edu.iut.app.ApplicationSession;
import edu.iut.app.Agenda;
import edu.iut.app.ExamEvent;
import edu.iut.gui.tools.FocusFrame;
/**
 * Panneau contenant la liste une liste d'�venement
 */
public class EventInfoListPanel extends JPanel  {
	private JScrollPane scroll;
	private JTable table;
	private DefaultTableModel tableModel;
	private Object[] cols = { "Date", ApplicationSession.getString("hour"), ApplicationSession.getString("intern"),
			"Info" };
	private int i;
	private Agenda agenda;
/**
 * Constructeur permettant d'utiliser une liste d'evenement donn� en parametre
 * @param agenda
 */
	public EventInfoListPanel(Agenda agenda) {
		super();
		this.agenda = agenda;
		Object[][] events = new Object[agenda.size()][cols.length];

		for (i = 0; i < agenda.size(); i++) {
			events[i][2] = agenda.get(i).getStudent().getFirstname() + " " + agenda.get(i).getStudent().getLastname();
			events[i][1] = agenda.get(i).getDate().getHours() + ":" + agenda.get(i).getDate().getMinutes();
			events[i][0] = agenda.get(i).getDate().getDate() + "/ " + (agenda.get(i).getDate().getMonth() + 1) + "/ "
					+ agenda.get(i).getDate().getYear();
			events[i][3] = "...";
		}

		tableModel = new DefaultTableModel(events, cols);

		table = new JTable(tableModel);
		table.getColumnModel().getColumn(3).setCellRenderer(new Button());
		table.getColumnModel().getColumn(3).setCellEditor(new ButtonEditor());

		table.setAutoCreateRowSorter(true);
		scroll = new JScrollPane(table);
		add(scroll);
	}
	/**
	 * Methode cr�er pour l'affichage du planning et le rafraichissement de son contenu
	 * @param agenda
	 */
	public void act(Agenda agenda) {
		String[][] events = new String[agenda.size()][cols.length];

		for (int i = 0; i < agenda.size(); i++) {
			events[i][2] = agenda.get(i).getStudent().getFirstname() + " " + agenda.get(i).getStudent().getLastname();
			events[i][1] = agenda.get(i).getDate().getHours() + ":" + agenda.get(i).getDate().getMinutes();
			events[i][0] = agenda.get(i).getDate().getDate() + "/ " + (agenda.get(i).getDate().getMonth() + 1) + "/ "
					+ agenda.get(i).getDate().getYear();
		}
		tableModel.setDataVector(events, cols);
		repaint();
		revalidate();
	}

	public void setColor(Color color,Color color2) {
		table.setBackground(color);
	}
/**
 * Objet cr�� localement permettant d'inserer un affichage style button dans un JTable
 */
	private class Button extends JButton implements TableCellRenderer {
		@Override
		public Component getTableCellRendererComponent(JTable arg0, Object arg1, boolean arg2, boolean arg3, int arg4,
				int arg5) {
			setText((arg1 == null) ? "" : arg1.toString());
			return this;
		}
	}
/**
 * Class permettant de recevoir de evenement de click dans un JTable
 */
	class ButtonEditor extends DefaultCellEditor {
		protected JButton btn;
		private String lbl;
		private Boolean clicked;
		private ExamEvent eventSelected;

		public ButtonEditor() {
			super(new JTextField());
			btn = new JButton();
			btn.setOpaque(true);

			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					fireEditingStopped();
				}
			});
		}

		@Override
		public Component getTableCellEditorComponent(JTable tableref, Object obj, boolean selected, int row, int col) {
			lbl = (obj == null) ? "" : obj.toString();
			btn.setText(lbl);
			clicked = true;
			eventSelected = agenda.get(row);
			return btn;
		}

		@Override
		public Object getCellEditorValue() {
			if (clicked)
				new FocusFrame(getComponent(), new EventEditPanel(eventSelected),
						ApplicationSession.getString("event") + "Inforamtion");
			clicked = false;
			return lbl;
		}
	}

}
