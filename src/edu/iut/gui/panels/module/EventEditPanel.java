package edu.iut.gui.panels.module;

import static edu.iut.gui.tools.Config.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.*;

import edu.iut.app.ApplicationSession;
import edu.iut.app.ExamEvent;
import edu.iut.app.Person;
import edu.iut.gui.tools.Colorable;
import edu.iut.gui.tools.Label;

/**
 * 
 */
public class EventEditPanel extends JPanel implements Colorable {

	ArrayList<JTextField> inputs;
	private ArrayList<JTextField> juryInput;
	private JButton btnSave;
	private ExamEvent event;

	public EventEditPanel() {
		super();
		setupUI(null);
	}

	public EventEditPanel(Date date) {
		super();
		setupUI(date);
	}

	public EventEditPanel(ExamEvent eventSelected) {
		this.event = eventSelected;
		setupUI(eventSelected.getDate());
	}

	/**
	 * Cr�ation du panel en filtrant, si le paramtre date est non null,
	 * pr�inscrit la date dans les champs date et heur
	 * 
	 * @param date
	 */
	private void setupUI(Date date) {
		setLayout(new BorderLayout(0, 10));

		JPanel titlepnl = new JPanel();
		titlepnl.add(new Label().setLabel(ApplicationSession.getString("event"), Font.BOLD, FONT_TITLE_2));

		JPanel form = buildForm(date);
		btnSave = new JButton(ApplicationSession.getString("save"));
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (event == null)
						ApplicationSession.instance().getAgenda().addEvent(getInfo());
					else
						event.setInfo(getInfo());
					SwingUtilities.getWindowAncestor(form).dispose();// (Component)e.getSource()
				} catch (Exception except) {
					JOptionPane.showMessageDialog(form, except.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);// (Component)e.getSource()
				}
			}
		});

		add(titlepnl, BorderLayout.PAGE_START);
		add(form, BorderLayout.CENTER);
		add(btnSave, BorderLayout.PAGE_END);
	}

	/**
	 * Fonction permettant la cr�ation du formulaire
	 * 
	 * @param date
	 * @return formulaire
	 */
	private JPanel buildForm(Date date) {
		String[] textlabels = { "Date (" + ApplicationSession.getString("dd/mm/yyyy") + ")",
				ApplicationSession.getString("time") + " (HH:MM)", ApplicationSession.getString("classroom"),
				ApplicationSession.getString("student") };
		// , "nb Jury", "Jury n�"
		 GridLayout formLayout = new GridLayout(textlabels.length, 2);
		formLayout.setVgap(10);
		formLayout.setHgap(10);
		JPanel formRight = new JPanel(formLayout);
		inputs = new ArrayList<>();

		for (int i = 0; i < textlabels.length; i++) {
			inputs.add(new JTextField(10));
			formRight.add(new JLabel(textlabels[i]));
			formRight.add(inputs.get(i));
		}
		if (date != null) {
			inputs.get(0).setText(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getYear());
			inputs.get(1).setText(date.getHours() + ":" + date.getMinutes());
		}
//		 formLayout = new GridLayout(textlabels.length, 2);
//		formLayout.setVgap(10);
//		formLayout.setHgap(10);

		juryInput = new ArrayList<>();
		Integer tab[] = { 1, 2, 3 };
		JPanel formLeft = new JPanel();
		formLeft.setLayout(new GridLayout(5, 1));
		formLeft.setSize(formRight.getSize());
		JComboBox<Integer> nbJuryList = new JComboBox<Integer>(tab);
		//nbJuryList.setSelectedIndex(1);
		nbJuryList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox<Integer> self = (JComboBox<Integer>) e.getSource();
				switch (((Integer) self.getSelectedItem())) {
					case 1 :
						if (juryInput.size() > 0) {
							juryInput.clear();
							formLeft.removeAll();
							formLeft.add(new JLabel("Nb Jury"));
							formLeft.add(nbJuryList);
						}
						JTextField tmp;
						juryInput.add(tmp = new JTextField(10));
						formLeft.add(new JLabel("Jury n�" + (juryInput.size())));
						formLeft.add(tmp);
						break;
					case 2 :
						if (juryInput.size() > 0) {
							juryInput.clear();
							formLeft.removeAll();
							formLeft.add(new JLabel("Nb Jury"));
							formLeft.add(nbJuryList);
						}
						for (int i=0; i<2; i++) {
							JTextField temp;
							juryInput.add(temp = new JTextField(10));
							formLeft.add(new JLabel("Jury n�" + (i+1)));
							formLeft.add(temp);
						}
						break;
					case 3 :
						if (juryInput.size() > 0) {
							juryInput.clear();
							formLeft.removeAll();
							formLeft.add(new JLabel("Nb Jury"));
							formLeft.add(nbJuryList);
						}
						for (int i=0; i<3; i++) {
							JTextField temp;
							juryInput.add(temp = new JTextField(10));
							formLeft.add(new JLabel("Jury n�" + (i+1)));
							formLeft.add(temp);
						}
						break;
				}
				revalidate();
				repaint();
			}
		});
		formLeft.add(new JLabel("Nb Jury"));
		formLeft.add(nbJuryList);
		JTextField tmp;
		juryInput.add(tmp = new JTextField(10));
		formLeft.add(new JLabel("Jury n�" + (juryInput.size())));
		formLeft.add(tmp);
		
		if (event != null) {
			inputs.get(2).setText(event.getClassroom());
			inputs.get(3).setText(event.getStudent().getFirstname() + " " + event.getStudent().getLastname());
			nbJuryList.setSelectedIndex(event.getJury().size() - 1);
			for (int i = 0; i < juryInput.size(); i++)
				juryInput.get(i)
						.setText(event.getJury().get(i).getFirstname() + " " + event.getJury().get(i).getLastname());

		}

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(formRight,BorderLayout.WEST);
		panel.add(formLeft,BorderLayout.EAST);

		return panel;
	}

	/**
	 * Recupere les informations inscrite par les utilisateur et les v�rifies
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	private ExamEvent getInfo() throws Exception {
		Date date = new Date(0);
		String dateString = new String();
		int cmp = 0;

		for (char c : inputs.get(0).getText().toCharArray()) {
			if (cmp > 2 && dateString.length() > 2)
				throw new Exception("Date invalide");

			if (c == '/' || c == ':' || c == ' ') {

				if (cmp == 0) {
					date.setDate(Integer.parseInt(dateString));
				} else if (cmp == 1) {
					date.setMonth(Integer.parseInt(dateString) - 1);
				}
				cmp++;
				dateString = new String();
			} else {
				dateString += c;
				if (dateString.length() == 4)
					date.setYear(Integer.parseInt(dateString));
			}
		}
		cmp = 0;
		String hourString = new String();
		for (char c : inputs.get(1).getText().toCharArray()) {

			if (hourString.length() > 2)
				throw new Exception("heure invalide");

			if (c == '/' || c == ':') {
				if (cmp == 0)
					date.setHours(Integer.parseInt(hourString));
				else if (cmp == 1)
					date.setMinutes(Integer.parseInt(hourString));
				cmp++;
				hourString = new String();
			} else
				hourString += c;

		}

		Person student = buildPerson(Person.PersonFunction.STUDENT, inputs.get(3).getText());

		ArrayList<Person> jurys = new ArrayList<>();
		for (JTextField input : juryInput)
			jurys.add(buildPerson(Person.PersonFunction.JURY, input.getText()));

		ExamEvent event = new ExamEvent(date, student, jurys, inputs.get(2).getText());
		return event;
	}

	/**
	 * fonction permettant la cr�ation d'un Objet personne
	 * 
	 * @param function
	 * @param input
	 * @return Person
	 * @throws Exception
	 */
	private Person buildPerson(Person.PersonFunction function, String input) throws Exception {
		if (input.isEmpty())
			throw new Exception("Champs " + function.toString() + " vide");

		char studentString[] = input.trim().toCharArray();
		int i = 0;
		String name[] = new String[2];
		name[i] = new String();
		try {
			for (char c : studentString) {
				if (c == ' ') {
					i++;
					name[i] = new String();
				} else
					name[i] += c;
			}
			if (i != 1)
				throw new Exception();
		} catch (Exception e) {
			throw new Exception("Nom du" + function + " invalide \n\"" + input + "\"");
		}

		return new Person(function, name[0], name[1]);
	}

	@Override
	public void setColor(Color color1, Color color2) {
		btnSave.setBackground(color1);
	}
}
