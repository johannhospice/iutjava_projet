package edu.iut.gui.panels.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import edu.iut.app.ApplicationSession;
import edu.iut.gui.panels.main.agenda.AgendaManagerPanel;
import edu.iut.gui.panels.main.agenda.AgendaManagerPanel.ViewType;
import edu.iut.gui.panels.module.EventEditPanel;
import edu.iut.gui.tools.Colorable;
import edu.iut.gui.tools.FocusFrame;
import edu.iut.gui.tools.Panel;
/**
 * Barre de navigation, pr�sente dans la fenetre principale
 */
public class NavbarPanel extends JPanel implements Colorable {
	public static JButton btnPrecedent, btnSuivant;
	private static JLabel dateLabel;
	private JButton btnCreerSoutenance;
	private JButton btnJour;
	private JButton btnSemaine;
	private JButton btnMois;
	private JButton btnPlanning;

	public NavbarPanel(AgendaManagerPanel agenda) {
		super();
		btnCreerSoutenance = new JButton("+");
		btnPrecedent = new JButton("<<");
		btnSuivant = new JButton(">>");
		btnJour = new JButton(ApplicationSession.getString("day").toUpperCase());
		btnSemaine = new JButton(ApplicationSession.getString("week").toUpperCase());
		btnMois = new JButton(ApplicationSession.getString("month").toUpperCase());
		btnPlanning = new JButton(ApplicationSession.getString("scheduler").toUpperCase());

		Panel btngroup = new Panel(btnCreerSoutenance, btnPrecedent, btnSuivant);
		Panel btnviewgroup = new Panel(btnJour, btnSemaine, btnMois, btnPlanning);

		btnSuivant.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (AgendaManagerPanel.getType()) {
				case DAY:
					AgendaManagerPanel.setDay(AgendaManagerPanel.getDay() + 1);
					break;
				case WEEK:
					AgendaManagerPanel.setNextWeek();
					break;
				case MONTH:
					AgendaManagerPanel.setMonth(AgendaManagerPanel.getMonth() + 1);
					break;
				}
				act();
			}
		});

		btnPrecedent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (AgendaManagerPanel.getType()) {
				case DAY:
					AgendaManagerPanel.setDay(AgendaManagerPanel.getDay() - 1);
					break;
				case WEEK:
					AgendaManagerPanel.setPreviousWeek();
					break;
				case MONTH:
					AgendaManagerPanel.setMonth(AgendaManagerPanel.getMonth() - 1);
					break;
				}
				act();
			}
		});

		btnCreerSoutenance.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new FocusFrame(((JButton) e.getSource()), new EventEditPanel(), "Nouvelle soutenance");
			}
		});

		btnPlanning.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				agenda.switchView(ViewType.SCHEDULER);
				act();
			}
		});

		btnJour.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				agenda.switchView(ViewType.DAY);
				act();
			}
		});

		btnSemaine.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				agenda.switchView(ViewType.WEEK);
				act();
			}
		});

		btnMois.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				agenda.switchView(ViewType.MONTH);
				act();
			}
		});
		dateLabel = new JLabel(AgendaManagerPanel.getDay() + " "
				+ ApplicationSession.instance().getMinMonths()[AgendaManagerPanel.getMonth()] + ". "
				+ AgendaManagerPanel.getYear());
		add(btngroup, BorderLayout.LINE_START);
		add(dateLabel, BorderLayout.CENTER);
		add(btnviewgroup, BorderLayout.LINE_END);
	}

	public static void act() {
		switch (AgendaManagerPanel.getType()) {
		case DAY:
			dateLabel.setText(AgendaManagerPanel.getDay() + " "
					+ ApplicationSession.instance().getMinMonths()[AgendaManagerPanel.getMonth()] + ". "
					+ AgendaManagerPanel.getYear());
			break;
		case WEEK:
			dateLabel
					.setText(AgendaManagerPanel.getFirstDayWeek() + " - "
							+ (AgendaManagerPanel.getFirstDayWeek() + 7)
									% AgendaManagerPanel.dom[AgendaManagerPanel.getMonth()]
							+ " " + ApplicationSession.instance().getMinMonths()[AgendaManagerPanel.getMonth()] + ". "
							+ AgendaManagerPanel.getYear());
			break;
		case MONTH:
			dateLabel.setText(ApplicationSession.instance().getMonths()[AgendaManagerPanel.getMonth()] + " "
					+ AgendaManagerPanel.getYear());
			break;
		default:
			break;
		}

		System.err.println(
				AgendaManagerPanel.getDay() + "/" + AgendaManagerPanel.getMonth() + "/" + AgendaManagerPanel.getYear());
	}

	@Override
	public void setColor(Color color1, Color color2) {
		btnCreerSoutenance.setBackground(color1);
		btnSuivant.setBackground(color1);
		btnPrecedent.setBackground(color1);
		btnJour.setBackground(color1);
		btnSemaine.setBackground(color1);
		btnMois.setBackground(color1);
		btnPlanning.setBackground(color1);
	}

}
