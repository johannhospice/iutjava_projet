package edu.iut.gui.panels.main.agenda;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import edu.iut.app.ApplicationSession;
import edu.iut.app.Agenda;
import edu.iut.app.ExamEvent;
import edu.iut.gui.panels.module.EventEditPanel;
import edu.iut.gui.panels.module.EventInfoListPanel;
import edu.iut.gui.tools.Colorable;
import edu.iut.gui.tools.FocusFrame;

/**
 * Un panel icluant un bouton permettant d'ajouter un evenement et une liste
 * d'�venement
 */

public class SpecEventInfoListPanel extends JPanel implements Colorable {
    private Color color;
    private JButton addEventBtn;

    public SpecEventInfoListPanel(Date date, Component e) {
        setupUI(date, e);
    }

    public SpecEventInfoListPanel(Date date, Component source, Color color1, Color color2) {
        setupUI(date, source);
        if (color1 != null && color2 != null) {
            this.color = color1;
            setColor(color1, color2);
        } else if (color1 != null) {
            this.color = color1;
            setColor(color1, null);
        }
    }

    private void setupUI(Date date, Component e) {

        JPanel panel = new JPanel(new BorderLayout());

        addEventBtn = new JButton("+");
        addEventBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EventEditPanel eventEditPane = new EventEditPanel(date);
                eventEditPane.setColor(color, null);
                FocusFrame f = new FocusFrame((JButton) e.getSource(), eventEditPane, "Cr�ation de soutenance");
                f.revalidate();
                f.pack();
            }
        });

        Agenda newAgenda = getAgendaDateInclude(date);
        panel.add(addEventBtn, BorderLayout.PAGE_START);
        panel.add(new EventInfoListPanel(newAgenda), BorderLayout.PAGE_END);
        new FocusFrame(e, panel, ApplicationSession.getString("Event") + "s");
    }

    /**
     * Retourne une liste d'�venement pour une date donn�e
     *
     * @param date
     * @return
     */
    public Agenda getAgendaDateInclude(Date date) {
        Agenda agenda = ApplicationSession.instance().getAgenda();
        Agenda newAgenda = new Agenda();
        for (int j = 0; j < agenda.size(); j++) {
            ExamEvent aEvent = agenda.get(j);
            if (date.getHours() == aEvent.getDate().getHours() && date.getDate() == aEvent.getDate().getDate()
                    && date.getMonth() == aEvent.getDate().getMonth() && date.getYear() == aEvent.getDate().getYear())
                newAgenda.add(aEvent);
        }
        return newAgenda;
    }

    @Override
    public void setColor(Color color1, Color color2) {
        this.color = color1;
        addEventBtn.setBackground(color1);
    }
}
