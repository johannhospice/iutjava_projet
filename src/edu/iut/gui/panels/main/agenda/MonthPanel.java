package edu.iut.gui.panels.main.agenda;

import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.*;

import edu.iut.app.ApplicationSession;
import edu.iut.gui.tools.Actualisable;
import edu.iut.gui.tools.Colorable;

public class MonthPanel extends JPanel implements Actualisable,Colorable {
	/** The currently-interesting year (not modulo 1900!) */
	protected int yy, mm, dd;

	/** The buttons to be displayed */
	protected JButton labs[][];

	/** The number of day squares to leave blank at the start of this month */
	protected int leadGap = 0;

	/** A Calendar object used throughout */
	Calendar calendar = new GregorianCalendar();

	/** Today's year */
	protected final int thisYear = calendar.get(Calendar.YEAR);

	/** Today's month */
	protected final int thisMonth = calendar.get(Calendar.MONTH) + 1;

	String[] months = ApplicationSession.instance().getMonths();

	public MonthPanel() {
		super();
		setYYMMDD(AgendaManagerPanel.getDay(), AgendaManagerPanel.getMonth(), AgendaManagerPanel.getYear());
		buildGUI();
		recompute();
	}
	
	public MonthPanel(int year, int month, int day) {
		super();
		setYYMMDD(day, month, year);
		buildGUI();
		recompute();
	}

	private void setYYMMDD(int year, int month, int today) {
		yy = year;
		mm = month;
		dd = today;
	}

	private void buildGUI() {
		setLayout(new GridLayout(7, 7));
		labs = new JButton[6][7];
		JButton b0;
		for (String day : ApplicationSession.instance().getMinDays()) {
			add(b0 = new JButton(day.toUpperCase()));
			b0.setEnabled(false);

		}

		for (int i = 0; i < 6; i++)
			for (int j = 0; j < 7; j++) {
				add(labs[i][j] = new JButton(""));
				labs[i][j].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String num = e.getActionCommand();
						if (!num.equals(""))
							setDayActive(Integer.parseInt(num));

					}
				});
			}
	}

	protected void recompute() {

		leadGap = new GregorianCalendar(yy, mm, 1).get(Calendar.DAY_OF_WEEK) - 1;

		int daysInMonth = AgendaManagerPanel.dom[mm];

		for (int i = 0; i < leadGap; i++) {
			labs[0][i].setText("");
			labs[0][i].setEnabled(false);
		}

		for (int i = 1; i <= daysInMonth; i++) {
			JButton b = labs[(leadGap + i - 1) / 7][(leadGap + i - 1) % 7];
			b.setText(Integer.toString(i));
			Date date = new Date(AgendaManagerPanel.getYear(), AgendaManagerPanel.getMonth(), Integer.parseInt(b.getText()));
			b.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					new SpecEventInfoListPanel(date, (JButton) e.getSource());
					act();
				}
			});
		}

		for (int i = leadGap + daysInMonth; i < 6 * 7; i++) {
			labs[(i) / 7][((i) % 7)].setText("");
			labs[(i) / 7][((i) % 7)].setEnabled(false);
		}

		if (thisYear == yy && mm == thisMonth)
			setDayActive(dd); // shade the box for today

		repaint();
	}

	public void setDayActive(int newDay) {
		if (mm == AgendaManagerPanel.getMonth())
			if (yy == AgendaManagerPanel.getYear())

				for (int i = 0; i < 5; i++)
					for (int j = 0; j < 5; j++)
						if (Integer.parseInt(labs[i][j].getText()) == AgendaManagerPanel.getDay())
							labs[i][j].setBackground(Color.BLUE);

		Component square = labs[(leadGap + newDay - 1) / 7][(leadGap + newDay - 1) % 7];
		square.setBackground(Color.green);
		square.repaint();
	}

	public void setPrimaryColor(Color color) {
		for (int i = 0; i < 6; i++)
			for (int j = 0; j < 7; j++)
				labs[i][j].setBackground(color);
	}

	public void setSecondColor(Color color) {
		for (int i = 0; i < 6; i++)
			for (int j = 0; j < 7; j++)
				if (labs[i][j].isEnabled())
					labs[i][j].setBackground(color);
	}

	@Override
	public void act() {
		leadGap = new GregorianCalendar(yy, mm, 1).get(Calendar.DAY_OF_WEEK) - 1;

		int daysInMonth = AgendaManagerPanel.dom[mm];

		for (int i = 1; i <= daysInMonth; i++) {
			Date date = new Date(yy, mm, i);
			JButton b = labs[(leadGap + i - 1) / 7][(leadGap + i - 1) % 7];
			b.removeActionListener(b.getActionListeners()[0]);
			b.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					new SpecEventInfoListPanel(date, (JButton) e.getSource());
					act();
				}
			});
		}

		if (thisYear == yy && mm == thisMonth)
			setDayActive(dd); // shade the box for today

		repaint();
	}

	@Override
	public void setColor(Color color1, Color color2) {
		// TODO Auto-generated method stub
		
	}
	
}