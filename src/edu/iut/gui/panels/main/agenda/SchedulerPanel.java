package edu.iut.gui.panels.main.agenda;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.Calendar;

import javax.swing.*;

import edu.iut.app.ApplicationSession;
import edu.iut.gui.panels.module.EventInfoListPanel;
import edu.iut.gui.tools.Actualisable;
import edu.iut.gui.tools.Colorable;
/**
 * Panel principale contenant une liste de  tous les evenements
 */
public class SchedulerPanel extends JPanel implements Actualisable, Colorable {

	private EventInfoListPanel listEventPanel;

	public SchedulerPanel() {
		setLayout(new BorderLayout());
		add(listEventPanel = new EventInfoListPanel(ApplicationSession.instance().getAgenda()),BorderLayout.CENTER);
	}

	@Override
	public void act() {
		listEventPanel.act(ApplicationSession.instance().getAgenda());
	}

	@Override
	public void setColor(Color color1, Color color2) {
		listEventPanel.setColor(color1,  color2);
	}
}
