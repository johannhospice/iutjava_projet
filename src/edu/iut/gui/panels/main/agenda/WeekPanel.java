package edu.iut.gui.panels.main.agenda;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JPanel;

import edu.iut.app.ApplicationSession;
import edu.iut.gui.tools.Actualisable;
import edu.iut.gui.tools.Colorable;

public class WeekPanel extends JPanel implements Actualisable,Colorable {
	
	private JButton[][] table;
	private JPanel pnlAffichage;
	private Date debut;
	private Date fin;
	
	public WeekPanel() {
		// TODO Auto-generated constructor stub
		super();
		String[] jours = ApplicationSession.instance().getMinDays();
		int[] jour = new int[7], mois = new int[7];
		table = new JButton[25][8];
		pnlAffichage = new JPanel(new GridLayout(25, 8));

		for (int i = 0; i < 25; i++) {
			for (int j = 0; j < 8; j++) {
				table[i][j] = new JButton();
				pnlAffichage.add(table[i][j]);
			}
		}
		int month = AgendaManagerPanel.getMonth();
		int year = AgendaManagerPanel.getYear();
		int day = AgendaManagerPanel.getDay();
		for (int j = 1; j < 8; j++) {
			if (day <=  AgendaManagerPanel.dom[month]) {
				jours[j - 1] += " " + day + "/" + (month + 1);
				jour[j-1] = day;
				mois[j-1] = month+1;
			}
			else {
				jours[j - 1] += " " + (day-AgendaManagerPanel.dom[month]) + "/" + (month + 2);
				jour[j-1] = day-AgendaManagerPanel.dom[month];
				mois[j-1] = month+2;
			}
			day++;
		}
		
		for (int j = 1; j < 8; j++) {
			table[0][j].setText(jours[j - 1].toUpperCase());
			table[0][j].setEnabled(false);
		}
		
		for (int i = 0; i < 25; i++) {
			if (i > 0)
				table[i][0].setText(i + ":00");
			table[i][0].setEnabled(false);
		}
		
		for (int i = 1; i < 25; i++) {
			for (int j = 1; j < 8; j++) {
				Date date = new Date(AgendaManagerPanel.getYear(), mois[j-1], jour[j-1], i, 0);
				table[i][j].addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						new SpecEventInfoListPanel(date, (JButton) e.getSource());
						act();
					}
				});
			}
		}
		debut = new Date(AgendaManagerPanel.getYear(), mois[0], jour[0]);
		fin = new Date(AgendaManagerPanel.getYear(), mois[6], jour[6]);
		setLayout(new BorderLayout());
		add(pnlAffichage);
	}
	
	public WeekPanel(Date debut) {
		// TODO Auto-generated constructor stub
		super();
		this.debut = debut;
		String[] jours = ApplicationSession.instance().getMinDays();
		int[] jour = new int[7], mois = new int[7];
		table = new JButton[25][8];
		pnlAffichage = new JPanel(new GridLayout(25, 8));

		for (int i = 0; i < 25; i++) {
			for (int j = 0; j < 8; j++) {
				table[i][j] = new JButton();
				pnlAffichage.add(table[i][j]);
			}
		}
		int month = debut.getMonth();
		int year = debut.getYear();
		int day = debut.getDay();
		for (int j = 1; j < 8; j++) {
			if (day <=  AgendaManagerPanel.dom[month]) {
				jours[j - 1] += " " + day + "/" + (month + 1);
				jour[j-1] = day;
				mois[j-1] = month+1;
			}
			else {
				jours[j - 1] += " " + (day-AgendaManagerPanel.dom[month]) + "/" + (month + 2);
				jour[j-1] = day-AgendaManagerPanel.dom[month];
				mois[j-1] = month+2;
			}
			day++;
		}
		
		for (int j = 1; j < 8; j++) {
			table[0][j].setText(jours[j - 1].toUpperCase());
			table[0][j].setEnabled(false);
		}
		
		for (int i = 0; i < 25; i++) {
			if (i > 0)
				table[i][0].setText(i + ":00");
			table[i][0].setEnabled(false);
		}
		
		for (int i = 1; i < 25; i++) {
			for (int j = 1; j < 8; j++) {
				Date date = new Date(AgendaManagerPanel.getYear(), mois[j-1], jour[j-1], i, 0);
				table[i][j].addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						new SpecEventInfoListPanel(date, (JButton) e.getSource());
						act();
					}
				});
			}
		}
		fin = new Date(AgendaManagerPanel.getYear(), mois[6], jour[6]);
		setLayout(new BorderLayout());
		add(pnlAffichage);
	}

	
	@Override
	public void setColor(Color color1, Color color2) {
		setPrimaryColor(color1);
		setSecondColor(color2);
	}

	public void setPrimaryColor(Color color) {
		for (int i = 1; i < 25; i++) {
			for (int j = 1; j < 8; j++) {
				table[i][j].setBackground(color);
			}
		}
	}

	public void setSecondColor(Color color) {
		for (int i = 0; i < 25; i++) {
			table[i][0].setBackground(color);
		}
		for (int j = 0; j < 8; j++) {
			table[0][j].setBackground(color);
		}
	}
	
	@Override
	public void act() {
		// TODO Auto-generated method stub
		
	}

	public Date getDebut() {
		return debut;
	}

	public void setDebut(Date debut) {
		this.debut = debut;
	}

	public Date getFin() {
		return fin;
	}

	public void setFin(Date fin) {
		this.fin = fin;
	}

}
