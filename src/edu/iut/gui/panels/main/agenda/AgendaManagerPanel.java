package edu.iut.gui.panels.main.agenda;

import static edu.iut.gui.tools.Config.*;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import edu.iut.gui.tools.Actualisable;
import edu.iut.gui.tools.Colorable;
import edu.iut.app.ApplicationSession;
import edu.iut.gui.frames.SchedulerFrame;
import edu.iut.gui.panels.main.NavbarPanel;

/**
 * Classe g�rant les 4 principaux affichages de l'agenda
 */
public class AgendaManagerPanel extends JPanel implements Colorable {

	public enum ViewType {
		DAY, WEEK, MONTH, SCHEDULER;
	}

	private static ViewType type;

	private static JScrollPane dayScroll;
	private static JScrollPane weekScroll;
	private static JScrollPane monthScroll;
	private static JScrollPane scheduleScroll;

	private static JScrollPane currentPanel;

	private static int day;
	private static int month;
	private static int year;

	public final static int dom[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	public AgendaManagerPanel() {
		super(new BorderLayout());
		Calendar.getInstance().setFirstDayOfWeek(Calendar.MONDAY);

		day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		month = Calendar.getInstance().get(Calendar.MONTH);
		year = Calendar.getInstance().get(Calendar.YEAR);

		type = ViewType.DAY;

		dayScroll = new JScrollPane(new DayPanel());
		weekScroll = new JScrollPane(new WeekPanel());
		monthScroll = new JScrollPane(new MonthPanel());
		scheduleScroll = new JScrollPane(new SchedulerPanel());
		currentPanel = dayScroll;
		add(dayScroll);
	}

	/**
	 * Permet de changer de panel d'affichage selon son type
	 * 
	 * @param type
	 */
	public void switchView(ViewType type) {
		AgendaManagerPanel.type = type;
		switch (type) {
		case DAY:
			switchView(dayScroll);
			break;
		case WEEK:
			switchView(weekScroll);
			break;
		case MONTH:
			switchView(monthScroll);
			break;
		case SCHEDULER:
			switchView(scheduleScroll);
			break;
		}
	}

	public void setColor(Color color1, Color color2) {
		((Colorable) dayScroll.getViewport().getComponents()[0]).setColor(color1, color2);
		((Colorable) weekScroll.getViewport().getComponents()[0]).setColor(color1, color2);
		((Colorable) monthScroll.getViewport().getComponents()[0]).setColor(color1, color2);
		((Colorable) scheduleScroll.getViewport().getComponents()[0]).setColor(color1, color2);
	}

	/**
	 * Change la fenetre d'affichage
	 * 
	 * @param newpnl
	 */
	private void switchView(JScrollPane newpnl) {

		if (currentPanel != newpnl && newpnl != null) {
			remove(currentPanel);
			add(newpnl);
			currentPanel = newpnl;

			if (currentPanel == scheduleScroll) {
				NavbarPanel.btnPrecedent.setEnabled(false);
				NavbarPanel.btnSuivant.setEnabled(false);
			} else {
				NavbarPanel.btnPrecedent.setEnabled(true);
				NavbarPanel.btnSuivant.setEnabled(true);
			}
			repaint();
			revalidate();
			((Actualisable) currentPanel.getViewport().getComponents()[0]).act();
		}
	}

	/**
	 * Assigne le jour courrant de l'application
	 * 
	 * @param i
	 */
	public static void setDay(int i) {
		day = i;
		if (day > dom[getMonth()]) {
			setMonth(month + 1);
			day = day - dom[getMonth()];
		} else if (day < 1) {
			setMonth(month - 1);
			day = Math.abs(dom[getMonth()] - day);
		}
		Calendar.getInstance().set(Calendar.YEAR, Calendar.MONTH, day);
		act();
	}

	/**
	 * Assigne le mois courrant de l'application
	 * 
	 * @param i
	 */
	public static void setMonth(int i) {
		month = i;
		if (month > 11) {
			year++;
			month = 0;
		} else if (month < 0) {
			year--;
			month = 11;
		}
		Calendar.getInstance().set(year, month, Calendar.DATE);
		AgendaManagerPanel.type = type;
		switch (type) {
		case DAY:
			
			break;
		case WEEK:
			
			break;
		case MONTH:
			monthScroll.getViewport().removeAll();
			monthScroll.getViewport().add(new MonthPanel(year, month, day));
			break;
		case SCHEDULER:
			
			break;
		}
		act();
	}
	
	public static void setNextWeek() {
		/*weekScroll.removeAll();
		Date d = ((WeekPanel)currentPanel.getViewport().getComponents()[0]).getFin();
		Date temp = new Date(d.getYear(), d.getMonth(), d.getDate()+1);
		weekScroll.add(new WeekPanel(temp));*/
	}
	
	public static void setPreviousWeek() {
		/*weekScroll.removeAll();
		Date d = ((WeekPanel)currentPanel.getViewport().getComponents()[0]).getDebut();
		Date temp = new Date(d.getYear(), d.getMonth(), d.getDate()+1);
		weekScroll.add(new WeekPanel(temp));*/
	}

	/**
	 * Assigne l'ann�e courrante de l'application
	 * 
	 * @param i
	 */
	public static void setYear(int i) {
		year = i;
		Calendar.getInstance().set(year, Calendar.MONTH, Calendar.DATE);
		act();
	}

	public static int getDay() {
		return day;
	}

	public static int getMonth() {
		return month;
	}

	public static int getYear() {
		return year;
	}

	/**
	 * Renvoie le jour de la semaine de 0 � 6
	 * 
	 * @return day
	 */
	public static int getDayOfWeek() {

		return (month + day + Integer.parseInt(("" + year).substring(2))
				+ Integer.parseInt(("" + year).substring(2)) / 4) % 7;
	}

	public static ViewType getType() {
		return type;
	}

	public static void act() {
		((Actualisable) dayScroll.getViewport().getComponents()[0]).act();
		((Actualisable) weekScroll.getViewport().getComponents()[0]).act();
		((Actualisable) monthScroll.getViewport().getComponents()[0]).act();
		((Actualisable) scheduleScroll.getViewport().getComponents()[0]).act();
	}

	public static int getFirstDayWeek() {
		return Calendar.getInstance().getFirstDayOfWeek();
	}

	/**
	 * Assigne la date d'apres une chaine de caractere donn�e en parametre
	 * 
	 * @param date
	 * @throws Exception
	 */
	public static void setDateString(String date) throws Exception {
		String dateString = new String();
		int cmp = 0;

		for (char c : date.toCharArray()) {
			if (cmp > 2 && dateString.length() > 2)
				throw new Exception("Date invalide");

			if (c == '/' || c == ':' || c == ' ') {

				if (cmp == 0) {
					setDay(Integer.parseInt(dateString));
				} else if (cmp == 1) {
					setMonth(Integer.parseInt(dateString) - 1);
				}
				cmp++;
				dateString = new String();
			} else {
				dateString += c;
				if (dateString.length() == 4)
					setYear(Integer.parseInt(dateString));
			}
		}
	}
}
