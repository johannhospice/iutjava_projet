package edu.iut.gui.panels.main.agenda;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.*;

import edu.iut.app.Agenda;
import edu.iut.app.ApplicationSession;
import edu.iut.app.ExamEvent;
import edu.iut.gui.listeners.CreateEventListener;
import edu.iut.gui.panels.module.EventEditPanel;
import edu.iut.gui.panels.module.EventInfoListPanel;
import edu.iut.gui.tools.Actualisable;
import edu.iut.gui.tools.Colorable;
import edu.iut.gui.tools.FocusFrame;

/**
 * Panel d'affichage des �venements par jours
 *
 * @author djihe
 */
public class DayPanel extends JPanel implements Actualisable, Colorable {

    private JButton[][] table;
    private Color color1, color2;

    public DayPanel() {
        super(new GridLayout(25, 2));

        table = new JButton[25][2];
        for (int i = 0; i < 25; i++)
            for (int j = 0; j < 2; j++) {
                table[i][j] = new JButton();
                add(table[i][j]);
                table[i][j].setEnabled(false);
            }
        table[0][0].setText(ApplicationSession.getString("hour").toUpperCase());

        for (int i = 1; i < 25; i++) {
            if (i < 10)
                table[i][0].setText("0" + (i - 1) + ":00");
            else
                table[i][0].setText((i - 1) + ":00");
        }

        for (int i = 1; i < 25; i++) {
            table[i][1].setEnabled(true);
        }
        act();
    }

    @Override
    public void act() {
        table[0][1].setText(AgendaManagerPanel.getDay() + "/" + (AgendaManagerPanel.getMonth() + 1));

        for (int i = 1; i < 25; i++) {
            Date date = new Date(AgendaManagerPanel.getYear(), AgendaManagerPanel.getMonth(),
                    AgendaManagerPanel.getDay(), i - 1, 0);
            table[i][1].setText("" + ApplicationSession.instance().getAgenda().sizeIncludeDate(date));
            //Si un ou des actionlistener ont été ajouté auparavent, tous les supprimer
            for (ActionListener l : table[i][1].getActionListeners())
                table[i][1].removeActionListener(l);

            table[i][1].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new SpecEventInfoListPanel(date, (JButton) e.getSource(), color1, color2);
                    act();
                }
            });
        }
    }

    @Override
    public void setColor(Color color1, Color color2) {
        this.color1 = color1;
        this.color2 = color2;
        setPrimaryColor(color1);
        setSecondColor(color2);
    }

    private void setPrimaryColor(Color color) {
        for (int i = 1; i < 25; i++) {
            table[i][1].setBackground(color);
        }
    }

    private void setSecondColor(Color color) {
        for (int i = 0; i < 25; i++) {
            table[i][0].setBackground(color);
        }
        table[0][1].setBackground(color);
    }

}
