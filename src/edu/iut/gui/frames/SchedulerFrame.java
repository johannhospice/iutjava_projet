package edu.iut.gui.frames;

import static edu.iut.gui.tools.Config.DEFAULT_WIDTH_FRAME;
import static edu.iut.gui.tools.Config.DEFAULT_HEIGHT_FRAME;

import java.awt.*;
import javax.swing.*;

import edu.iut.gui.panels.main.agenda.AgendaManagerPanel;
import edu.iut.gui.tools.Actualisable;
import edu.iut.app.ApplicationSession;
import edu.iut.gui.module.MenuBar;
import edu.iut.gui.panels.main.NavbarPanel;

/**
 * Fenetre de principale de l'application
 */
@SuppressWarnings("serial")
public class SchedulerFrame extends JFrame implements Actualisable {

	private AgendaManagerPanel agendapnl;
	private static SchedulerFrame self;

	private SchedulerFrame() {
		super("IUT" + ApplicationSession.getString("Scheduler"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setMinimumSize(new Dimension(DEFAULT_WIDTH_FRAME, DEFAULT_HEIGHT_FRAME));
	}

	/**
	 * Conception du contenue de l'interface
	 */
	public void setupUI() {
		agendapnl = new AgendaManagerPanel();
		MenuBar menuBar = new MenuBar();
		NavbarPanel navbar = new NavbarPanel(agendapnl);

		agendapnl.setColor(Color.WHITE, Color.darkGray);
		navbar.setColor(Color.WHITE, Color.darkGray);
		menuBar.setColor(Color.WHITE);

		setJMenuBar(menuBar);
		add(navbar, BorderLayout.PAGE_START);
		add(agendapnl, BorderLayout.CENTER);
		pack();
	}

	public void rebuild() {
		getContentPane().removeAll();
		ApplicationSession.instance().loadString();
		setTitle("IUT " + ApplicationSession.getString("Scheduler"));
		setupUI();
		validate();
	}

	public void act() {
		AgendaManagerPanel.act();
	}

	/**
	 * Singleton
	 * 
	 * @return self
	 */
	public static SchedulerFrame instance() {
		if (self == null)
			self = new SchedulerFrame();
		return self;
	}

}
