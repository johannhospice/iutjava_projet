package edu.iut.gui.listeners;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import edu.iut.gui.frames.SchedulerFrame;
import edu.iut.gui.panels.module.EventEditPanel;
import edu.iut.gui.tools.FocusFrame;

import javax.swing.*;

public class CreateEventListener implements ActionListener {

	private final Color color;
	private Date date;

	public CreateEventListener(Date date,Color color) {
		this.date = date;
		this.color = color;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		EventEditPanel eventEditPane = new EventEditPanel(date);
		eventEditPane.setColor(color, null);
		new FocusFrame((JButton) e.getSource(), eventEditPane, "Cr�ation de soutenance");
	}
}
