package edu.iut.gui.listeners;

import edu.iut.app.ApplicationSession;
import edu.iut.io.XMLProjectWriter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by djihe on 17/01/2016.
 */
public class SaveListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent arg0) {

        try {
            XMLProjectWriter.save(ApplicationSession.instance().getAgenda());
            JOptionPane.showMessageDialog((Component) arg0.getSource(),
                    ApplicationSession.getString("save") + " complete", "Information",
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog((Component) arg0.getSource(), "save abort!\n" + e.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
