package edu.iut.gui.listeners;

import edu.iut.app.ApplicationSession;
import edu.iut.app.Person;
import edu.iut.app.Agenda;
import edu.iut.app.ExamEvent;
import edu.iut.app.Person;
import edu.iut.app.Person.*;
import edu.iut.gui.panels.module.EventInfoListPanel;
import edu.iut.gui.tools.FocusFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SearchListener implements ActionListener {
    private Person.PersonFunction function;

    public SearchListener(PersonFunction function) {
        this.function = function;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        String name[] = new String[2];
        try {
            String input = JOptionPane.showInputDialog((Component) arg0.getSource(),
                    new Label(ApplicationSession.getString(function.toString()) + ":"),
                    ApplicationSession.getString("search") + " "
                            + ApplicationSession.getString(function.toString()).toLowerCase(),
                    JOptionPane.DEFAULT_OPTION);

            if (input == null)
                return;
            int i = 0;
            name[i] = new String();
            for (char c : input.trim().toCharArray()) {
                if (c == ' ') {
                    i++;
                    name[i] = new String();
                } else
                    name[i] += c;
            }
            if (i != 1)
                throw new Exception("Nom du invalide \n\"" + input + "\"");

            Agenda agenda = new Agenda();
            if (function == PersonFunction.STUDENT) {
                for (ExamEvent e : ApplicationSession.instance().getAgenda())
                    if (name[0].equals(e.getStudent().getFirstname()))
                        if (name[1].equals(e.getStudent().getLastname()))
                            agenda.add(e);
            } else
                for (ExamEvent e : ApplicationSession.instance().getAgenda())
                    for (Person p : e.getJury())
                        if (p.getFirstname().equals(name[0]))
                            if (p.getLastname().equals(name[1]))
                                agenda.add(e);

            new FocusFrame((Component) arg0.getSource(), new EventInfoListPanel(agenda),
                    ApplicationSession.getString("search"));

        } catch (Exception e) {
            JOptionPane.showMessageDialog((Component) arg0.getSource(), e.getMessage(), "Erreur",
                    JOptionPane.ERROR_MESSAGE);
        }

    }
}