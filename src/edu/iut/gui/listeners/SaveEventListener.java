package edu.iut.gui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import edu.iut.app.ApplicationSession;
import edu.iut.app.ExamEvent;

public class SaveEventListener implements ActionListener {
	private ExamEvent event;
	
	public SaveEventListener(ExamEvent event){
		this.event=event;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ApplicationSession.instance().getAgenda().addEvent(event);
	}

}
