package edu.iut.gui.listeners;

import edu.iut.app.ApplicationSession;
import edu.iut.gui.panels.main.NavbarPanel;
import edu.iut.gui.panels.main.agenda.AgendaManagerPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GotoListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        String date = JOptionPane.showInputDialog((Component) e.getSource(),
                new edu.iut.gui.tools.Label("Date: (" + ApplicationSession.getString("DD/MM/YYYY") + ")"),
                ApplicationSession.getString("Goto"), JOptionPane.DEFAULT_OPTION);
        if (date == null)
            return;
        try {
            AgendaManagerPanel.setDateString(date);
            AgendaManagerPanel.act();
            NavbarPanel.act();
        } catch (Exception except) {
            JOptionPane.showMessageDialog((Component) e.getSource(), except.getMessage(), "Erreur",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
