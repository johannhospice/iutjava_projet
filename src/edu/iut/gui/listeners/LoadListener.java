package edu.iut.gui.listeners;

import edu.iut.app.ApplicationSession;
import edu.iut.gui.frames.SchedulerFrame;
import edu.iut.gui.panels.main.agenda.AgendaManagerPanel;
import edu.iut.io.XMLProjectReader;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by djihe on 17/01/2016.
 */
public class LoadListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {

        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

            if (new File(System.getProperty("user.dir") + "/backup").isDirectory())
                fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir") + "/backup"));
            else
                fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("xml file", "xml"));
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setDialogTitle("Import xml save file");
            int returnVal = fileChooser.showOpenDialog(SchedulerFrame.instance());

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                java.io.File file = fileChooser.getSelectedFile();
                XMLProjectReader.load(file);
                JOptionPane.showMessageDialog(null, ApplicationSession.getString("load") + " complete", "info",
                        JOptionPane.INFORMATION_MESSAGE);
                AgendaManagerPanel.act();
            }
        } catch (Exception execpt) {
            execpt.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    ApplicationSession.getString("load") + " "
                            + ApplicationSession.getString("abort").toLowerCase() + "\ncause: "
                            + execpt.getMessage(),
                    "error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
