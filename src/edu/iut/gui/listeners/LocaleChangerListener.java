package edu.iut.gui.listeners;

import edu.iut.app.ApplicationSession;
import edu.iut.gui.frames.SchedulerFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;

public class LocaleChangerListener implements ActionListener {
    private final Locale locale;

    public LocaleChangerListener(Locale locale) {
        this.locale = locale;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        ApplicationSession.instance().setLocale(locale);
        SchedulerFrame.instance().rebuild();
    }

}
