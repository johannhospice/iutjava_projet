package edu.iut.io;

import java.io.File;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import edu.iut.app.Agenda;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.iut.app.ExamEvent;
import edu.iut.app.Person;
/**
 * Permet de sauvegarder un fichier donn� en paramtre
 */
public class XMLProjectWriter {

    public static void save(Agenda data) throws Exception {
        File dir = new File("backup");

        if (!dir.isDirectory()) {
            if (!dir.mkdir()) throw new Exception("backup mkdir abord");
        }
        File xmlfile =new File(System.getProperty("user.dir") + 
            "/backup/save-" + new Date().getTime() + ".xml");


        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        // creation parseur et document
        final DocumentBuilder builder = factory.newDocumentBuilder();
        final Document document = builder.newDocument();

        // creation de l'element racine
        final Element examevent = document.createElement("examevent");
        document.appendChild(examevent);

        // creation de l'�v�nement
        for (ExamEvent eventcurrent : data) {

            final Element event = document.createElement("event");
            event.setAttribute("date", String.valueOf(eventcurrent.getDate().getTime()));
            event.setAttribute("classroom", eventcurrent.getClassroom());

            // Ajout etudiant
            final Element student = getPersonElement(document, eventcurrent.getStudent());
            event.appendChild(student);

            // Ajout jurys
            final Element eventJurys = document.createElement("jurys");
            for (Person person : eventcurrent.getJury()) {
                final Element eventJury = getPersonElement(document, person);
                eventJurys.appendChild(eventJury);
            }
            event.appendChild(eventJurys);

            // document
            final Element eventDocs = document.createElement("documents");
            for (edu.iut.app.Document doc : eventcurrent.getDocuments()) {
                final Element eventDoc = document.createElement("document");
                eventDoc.setAttribute("URI", doc.getDocumentURI());
                eventDocs.appendChild(eventDoc);
            }
            event.appendChild(eventDocs);
            examevent.appendChild(event);
        }

        final TransformerFactory transformerFactory = TransformerFactory
                .newInstance();
        final Transformer transformer = transformerFactory.newTransformer();
        final DOMSource source = new DOMSource(document);
        final StreamResult sortie = new StreamResult(xmlfile);

        // final StreamResult result = new StreamResult(System.out);

        // prologue
        transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        // formatage
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "2");

        // sortie
        transformer.transform(source, sortie);
    }

    private static Element getPersonElement(Document document, Person person) {
        final Element personE = document.createElement("person");
        personE.setAttribute("function", person.getFunction().toString());
        personE.setAttribute("firstname", person.getFirstname());
        personE.setAttribute("lastname", person.getLastname());
        personE.setAttribute("email", person.getEmail());
        personE.setAttribute("phone", person.getPhone());
        return personE;
    }
}
