package edu.iut.io;

import edu.iut.app.Agenda;
import edu.iut.app.ExamEvent;
import edu.iut.app.Person;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Permet de charger un fichier xml donn�e en parametre dans les donn�es de
 * l'application
 *
 */
public class XMLProjectReader {
	public static Agenda load(java.io.File xmlfile) throws Exception {

		Agenda data = new Agenda();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(xmlfile);

		final NodeList events = document.getDocumentElement().getChildNodes();

		for (int i = 0; i < events.getLength(); i++) {

			if (events.item(i).getNodeType() == Node.ELEMENT_NODE) {

				final Element event = (Element) events.item(i);
				Date date = new Date(Long.valueOf(event.getAttributes().getNamedItem("date").getNodeValue()));
				String classRoom = new String(event.getAttributes().getNamedItem("classroom").getNodeValue());

				final Element studentNode = (Element) event.getElementsByTagName("person").item(0);
				Person student = getPerson(studentNode);

				ArrayList<Person> jurys = new ArrayList<>();
				final NodeList jurysList = event.getElementsByTagName("person");
				for (int j = 1; j < jurysList.getLength(); j++) {
					if (events.item(i).getNodeType() == Node.ELEMENT_NODE) {
						final Element juryElement = (Element) jurysList.item(j);
						jurys.add(getPerson(juryElement));
					}
				}

				ArrayList<edu.iut.app.Document> documents = new ArrayList<>();
				final NodeList documentsNode = event.getElementsByTagName("document");
				for (int j = 0; j < documentsNode.getLength(); j++) {
					documents.add(
							new edu.iut.app.Document(((Element) documentsNode.item(j)).getAttribute("URI")));
				}

				data.add(new ExamEvent(date, student, jurys, classRoom, documents));
			}
		}
		return data;
	}

	private static Person getPerson(Element personElement) {
		Person.PersonFunction function;
		switch (personElement.getAttribute("function")) {
		case "Jury":
			function = Person.PersonFunction.JURY;
			break;
		case "Student":
			function = Person.PersonFunction.STUDENT;
			break;
		default:
			function = Person.PersonFunction.NONE;
			break;
		}
		Person person = new Person(function, personElement.getAttribute("firstname"),
				personElement.getAttribute("lastname"), personElement.getAttribute("email"),
				personElement.getAttribute("phone"));

		return person;
	}
}
