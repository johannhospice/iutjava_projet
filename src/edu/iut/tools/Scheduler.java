package edu.iut.tools;

import edu.iut.app.ApplicationSession;
import edu.iut.gui.frames.SchedulerFrame;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * Classe de lancement de l'application
 */
public class Scheduler {

	public static void main(String[] args) {
		ApplicationSession.instance();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				SchedulerFrame.instance().setVisible(true);
				SchedulerFrame.instance().setupUI();
			}
		});
	}
}
